import os
import sys
import shutil
import configparser
from copy import copy
from unittest import mock, TestCase


class CloudformationDeployTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region', 'IMAGE_NAME': 'test-image',
                                  'AWS_OIDC_ROLE_ARN': ''})
    def test_discover_auth_method_default_credentials_only(self):
        from pipe import pipe

        cloudformation_pipe = pipe.ECRPush(schema=pipe.schema, check_for_newer_version=True)
        cloudformation_pipe.auth()
        self.assertEqual(cloudformation_pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_DEFAULT_REGION': 'test-region', 'IMAGE_NAME': 'test-image'})
    def test_discover_auth_method_oidc_only(self):
        from pipe import pipe

        cloudformation_pipe = pipe.ECRPush(schema=pipe.schema, check_for_newer_version=True)
        cloudformation_pipe.auth()
        self.assertEqual(cloudformation_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region', 'IMAGE_NAME': 'test-image'})
    def test_discover_auth_method_oidc_and_default_credentials(self):
        from pipe import pipe
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        cloudformation_pipe = pipe.ECRPush(schema=pipe.schema, check_for_newer_version=True)
        cloudformation_pipe.auth()

        self.assertEqual(cloudformation_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)
