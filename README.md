# Bitbucket Pipelines Pipe: AWS ECR push image

Pushes docker images to the AWS Elastic Container Registry.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-ecr-push-image:1.5.0
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    IMAGE_NAME: "<string>"
    # TAGS: "<string>" # Optional
    # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**) | AWS access key id |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key |
| AWS_DEFAULT_REGION (**) | AWS region |
| AWS_OIDC_ROLE_ARN     |  The ARN of the role used for web identity federation or OIDC. See **Authentication**. |
| IMAGE_NAME            | The name of the image to push to the ECR. The name should be the same as your ECR repository name (example: `my-ecr-image`). Remember that you don't need to add your registry URL in front of the image name, the pipe will fetch this URL from AWS and add it for you. Don't add tag in the IMAGE_NAME variable, pass variables `TAGS` instead (see more information below). |
| TAGS                  | List of white space separated tags to push. Default: `latest`.|
| ECR_PUBLIC            | Turn on to push ecr-public registries. Default: `false`. ECR Public only works in "us-east-1" region, so the AWS_DEFAULT_REGION will be omitted when setting ECR_PUBLIC to `true`. |
| PUBLIC_REGISTRY_ALIAS | Registry alias used to push images to ECR Public Registry. Required if ECR_PUBLIC is set to `true`. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have a IAM user configured with programmatic access or Web Identity Provider (OIDC) role, with the necessary permissions to push docker images to your ECR repository. You also need to set up an
ECR container registry if you don't already have on. Here is a [AWS ECR Getting started](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_GetStarted.html) guide from AWS on how to set up a new registry.

IMPORTANT! This pipe expects the docker image to be built already. You can see the examples below for more details.


## Authentication

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you set up OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * set up a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on AWS ECR resources


## Examples

Useful tip: you can use [pipelines caching](https://confluence.atlassian.com/bitbucket/caching-dependencies-895552876.html) to cache docker layers created in previous builds. See the **Docker layer caching** section in 
[Run Docker commands in Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/run-docker-commands-in-bitbucket-pipelines-879254331.html)


### Basic example:

Building and pushing the image with default options:

```yaml
script:
  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push the image to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.5.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
```

Building and pushing the ecr-public image with default options:

```yaml
script:
  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push the image to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.5.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      IMAGE_NAME: my-docker-image
      ECR_PUBLIC: "true"
      PUBLIC_REGISTRY_ALIAS: registry-alias
```

Building and pushing the image with a tag named as current Bitbucket branch that can be set as ${BITBUCKET_BRANCH}:

```yaml
script:
  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push the image to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.5.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
      TAGS: '${BITBUCKET_BRANCH}'
```

Example building and pushing the image with default options. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push the image to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.5.0
    variables:
      IMAGE_NAME: my-docker-image
```


### Advanced example:

Build and push the image with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:

```yaml
- step:
    oidc: true
    script:
      # build the image
      - docker build -t my-docker-image .

      # use the pipe to push the image to AWS ECR
      - pipe: atlassian/aws-ecr-push-image:1.5.0
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          IMAGE_NAME: my-docker-image
```


Pushing multiple tags: `${BITBUCKET_TAG}` and `latest`:
to tag the image:

```yaml
script:

  # build the image
  - docker build -t my-docker-image .

  # use the pipe to push to AWS ECR
  - pipe: atlassian/aws-ecr-push-image:1.5.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      IMAGE_NAME: my-docker-image
      TAGS: '${BITBUCKET_TAG} latest'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,ecr
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
